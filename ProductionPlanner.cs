﻿using System;
using System.Linq;
using ASD.Graphs;

namespace ASD
{
    public class ProductionPlanner : MarshalByRefObject
    {
        /// <summary>
        /// Flaga pozwalająca na włączenie wypisywania szczegółów skonstruowanego planu na konsolę.
        /// Wartość <code>true</code> spoeoduje wypisanie planu.
        /// </summary>
        public bool ShowDebug { get; } = false;
        
        /// <summary>
        /// Część 1. zadania - zaplanowanie produkcji telewizorów dla pojedynczego kontrahenta.
        /// </summary>
        /// <remarks>
        /// Do przeprowadzenia testów wyznaczających maksymalną produkcję i zysk wymagane jest jedynie zwrócenie obiektu <see cref="PlanData"/>.
        /// Testy weryfikujące plan wymagają przypisania tablicy z planem do parametru wyjściowego <see cref="weeklyPlan"/>.
        /// </remarks>
        /// <param name="production">
        /// Tablica obiektów zawierających informacje o produkcji fabryki w kolejnych tygodniach.
        /// Wartości pola <see cref="PlanData.Quantity"/> oznaczają limit produkcji w danym tygodniu,
        /// a pola <see cref="PlanData.Value"/> - koszt produkcji jednej sztuki.
        /// </param>
        /// <param name="sales">
        /// Tablica obiektów zawierających informacje o sprzedaży w kolejnych tygodniach.
        /// Wartości pola <see cref="PlanData.Quantity"/> oznaczają maksymalną sprzedaż w danym tygodniu,
        /// a pola <see cref="PlanData.Value"/> - cenę sprzedaży jednej sztuki.
        /// </param>
        /// <param name="storageInfo">
        /// Obiekt zawierający informacje o magazynie.
        /// Wartość pola <see cref="PlanData.Quantity"/> oznacza pojemność magazynu,
        /// a pola <see cref="PlanData.Value"/> - koszt przechowania jednego telewizora w magazynie przez jeden tydzień.
        /// </param>
        /// <param name="weeklyPlan">
        /// Parametr wyjściowy, przez który powinien zostać zwrócony szczegółowy plan sprzedaży.
        /// </param>
        /// <returns>
        /// Obiekt <see cref="PlanData"/> opisujący wyznaczony plan.
        /// W polu <see cref="PlanData.Quantity"/> powinna znaleźć się maksymalna liczba wyprodukowanych telewizorów,
        /// a w polu <see cref="PlanData.Value"/> - wyznaczony maksymalny zysk fabryki.
        /// </returns>
        public PlanData CreateSimplePlan(PlanData[] production, PlanData[] sales, PlanData storageInfo,
            out SimpleWeeklyPlan[] weeklyPlan)
        {
            weeklyPlan = null;
            int n = production.Length;
            weeklyPlan = new SimpleWeeklyPlan[n];
            for(int i = 0; i < n; ++i)
            {
                weeklyPlan[i].UnitsProduced = 0;
                weeklyPlan[i].UnitsSold = 0;
                weeklyPlan[i].UnitsStored = 0;
            }
            //Graph tmp = new AVLAdjacencyList(t)
            int k = n + 2;
            int source = 0; int sink = n + 1;
            Graph g = new AdjacencyListsGraph<AVLAdjacencyList>(true, k);
            Graph r = new AdjacencyListsGraph<AVLAdjacencyList>(true, k);
            

            for (int j = 0; j < n-1; ++j)
            {
                g.AddEdge(new Edge(source, j+1, production[j].Quantity)); // dodawanie produkcji      //produkcja+,   sales-,  magazynowanie-
                r.AddEdge(new Edge(source, j+1, production[j].Value));

                g.AddEdge(new Edge(j+1, sink, sales[j].Quantity)); // dodawanie sprzedazy
                r.AddEdge(new Edge(j+1, sink, -sales[j].Value));

                g.AddEdge(new Edge(j + 1, j + 2, storageInfo.Quantity)); // dodawanie magazynu
                r.AddEdge(new Edge(j + 1, j + 2, storageInfo.Value));
            }
            
            g.AddEdge(source, n, production[n - 1].Quantity); // ostatni tydzien produkcji
            r.AddEdge(source, n, production[n - 1].Value);
            g.AddEdge(n, sink, sales[n - 1].Quantity); // ostatni tydzien sprzedazy
            r.AddEdge(n, sink, -sales[n - 1].Value);
            (double value, double cost, Graph flow) ret = MinCostFlowGraphExtender.MinCostFlow(g, r, source, sink, false, null, null, false);
            int produced = 0;
            for (int i = 0; i < n; ++i)
            {
                produced += (int)ret.flow.GetEdgeWeight(source, i + 1);
                weeklyPlan[i].UnitsProduced = (int)ret.flow.GetEdgeWeight(source, i + 1);
                weeklyPlan[i].UnitsSold = (int)ret.flow.GetEdgeWeight(i + 1, sink);
                if (i != n - 1)
                    weeklyPlan[i].UnitsStored = (int)ret.flow.GetEdgeWeight(i + 1, i + 2);
                else
                    weeklyPlan[i].UnitsStored = 0;
             }
            return new PlanData {Quantity = produced, Value = -ret.cost};
        }

        /// <summary>
        /// Część 2. zadania - zaplanowanie produkcji telewizorów dla wielu kontrahentów.
        /// </summary>
        /// <remarks>
        /// Do przeprowadzenia testów wyznaczających produkcję dającą maksymalny zysk wymagane jest jedynie zwrócenie obiektu <see cref="PlanData"/>.
        /// Testy weryfikujące plan wymagają przypisania tablicy z planem do parametru wyjściowego <see cref="weeklyPlan"/>.
        /// </remarks>
        /// <param name="production">
        /// Tablica obiektów zawierających informacje o produkcji fabryki w kolejnych tygodniach.
        /// Wartość pola <see cref="PlanData.Quantity"/> oznacza limit produkcji w danym tygodniu,
        /// a pola <see cref="PlanData.Value"/> - koszt produkcji jednej sztuki.
        /// </param>
        /// <param name="sales">
        /// Dwuwymiarowa tablica obiektów zawierających informacje o sprzedaży w kolejnych tygodniach.
        /// Pierwszy wymiar tablicy jest równy liczbie kontrahentów, zaś drugi - liczbie tygodni w planie.
        /// Wartości pola <see cref="PlanData.Quantity"/> oznaczają maksymalną sprzedaż w danym tygodniu,
        /// a pola <see cref="PlanData.Value"/> - cenę sprzedaży jednej sztuki.
        /// Każdy wiersz tablicy odpowiada jednemu kontrachentowi.
        /// </param>
        /// <param name="storageInfo">
        /// Obiekt zawierający informacje o magazynie.
        /// Wartość pola <see cref="PlanData.Quantity"/> oznacza pojemność magazynu,
        /// a pola <see cref="PlanData.Value"/> - koszt przechowania jednego telewizora w magazynie przez jeden tydzień.
        /// </param>
        /// <param name="weeklyPlan">
        /// Parametr wyjściowy, przez który powinien zostać zwrócony szczegółowy plan sprzedaży.
        /// </param>
        /// <returns>
        /// Obiekt <see cref="PlanData"/> opisujący wyznaczony plan.
        /// W polu <see cref="PlanData.Quantity"/> powinna znaleźć się optymalna liczba wyprodukowanych telewizorów,
        /// a w polu <see cref="PlanData.Value"/> - wyznaczony maksymalny zysk fabryki.
        /// </returns>
        public PlanData CreateComplexPlan(PlanData[] production, PlanData[,] sales, PlanData storageInfo,
            out WeeklyPlan[] weeklyPlan)
        {
            weeklyPlan = null;
            if (storageInfo.Value == 0 && storageInfo.Quantity == 0)
                return new PlanData { Quantity = 666, Value = 666 };
            int count = sales.GetLength(0); // count ilosc kotrahentow
            int n = production.Length; // n ilosc tygodni
            int source = n;
            int sink = n + 1;
            int start = n + 2; // nr pierwszego kontrahenta
            int end = start + count - 1; // nr ostatniego kontrahenta
            int V = n + 2 + count * n; // ilosc wierzcholkow
            Graph g = new AdjacencyListsGraph<AVLAdjacencyList>(true, V);
            Graph r = new AdjacencyListsGraph<AVLAdjacencyList>(true, V);
            int quantity = 0;
            //bool first = true;
            for(int i = start; i < V; ++i) // polaczenie wierzcholkow do sinka
            {
                g.AddEdge(i, sink, 0);//Double.MaxValue);
                r.AddEdge(i, sink, 0);
            }
            for(int j = 0; j < n; ++j)
            {
                g.AddEdge(source, j, production[j].Quantity);
                r.AddEdge(source, j, production[j].Value);

                int buyer = 0;
                for (int i = start; i <= end; ++i)
                {
                    
                    for (int k = j; k < n; ++k)
                    {
                        //Console.WriteLine("j:{0} , n:{1} , i: {2} , {3}, {4}, {5}", j, n, i, i, i, i );
                        double storageCost = (k - j) * storageInfo.Value;
                        double realCost = production[j].Value + storageCost;
                        if (realCost < sales[buyer, j].Value)
                        {
                            g.AddEdge(j, i + k * count, sales[buyer,k].Quantity);//production[j].Quantity);
                            r.AddEdge(j, i + k * count, -sales[buyer, k].Value + storageCost);// realCost);

                            //r.AddEdge(j, i + k * count, -sales[buyer,k].Value);// realCost);
                        }
                        //Console.WriteLine("sukces");
                    }
                    buyer++;
                }
            }
            (double value, double cost, Graph flow) ret = MinCostFlowGraphExtender.MinCostFlow(g, r, source, sink, false, null, null, false);
            int produced = 0;
            for (int i = 0; i < n; ++i)
            {
                produced += (int)ret.flow.GetEdgeWeight(source, i + 1);

                //produced += (int)ret.flow.GetEdgeWeight(source, i + 1);
                //weeklyPlan[i].UnitsProduced = (int)ret.flow.GetEdgeWeight(source, i);
                //foreach (Edge e in ret.flow.OutEdges(i))
                //{
                //    weeklyPlan[i].UnitsSold[(e.To - n - 2) % count] += (int)e.Weight;
                //}
                //weeklyPlan[i].UnitsSold = (int)ret.flow.GetEdgeWeight(i + 1, sink);
                //weeklyPlan[i].UnitsStored;    
                //if (i != n - 1)
                //    weeklyPlan[i].UnitsStored = (int)ret.flow.GetEdgeWeight(i + 1, i + 2);
                //else
                //    weeklyPlan[i].UnitsStored = 0;
            }
            return new PlanData { Quantity = produced, Value = -ret.cost };
            //int count = sales.GetLength( 0);
            //int n = production.Length;
            //int k = n + 2 + count;
            //int source = n;
            //int sink = n + 1;
            //int start = n + 2; // index pierwszego kontrahenta
            //int end = start + count - 1; // index v ostatniego kontrahenta 
            //Graph g = new AdjacencyListsGraph<AVLAdjacencyList>(true, k);
            //Graph r = new AdjacencyListsGraph<AVLAdjacencyList>(true, k);
            //weeklyPlan = new WeeklyPlan[n];
            //for (int i = 0; i < n; ++i)
            //    weeklyPlan[i].UnitsSold = new int[n];
            //for (int j = 0; j < n - 1; ++j)
            //{
            //    g.AddEdge(new Edge(source, j, production[j].Quantity)); // dodawanie produkcji      //produkcja+,   sales-,  magazynowanie-
            //    r.AddEdge(new Edge(source, j, production[j].Value));
            //    int pp = 0;
            //    for (int i = start; i <= end; ++i)
            //    {
            //        g.AddEdge(j, i, sales[pp, j].Quantity);
            //        r.AddEdge(j, i, -sales[pp,j].Value);
            //        g.AddEdge(new Edge(j, sink, sales[pp,j].Quantity)); // dodawanie sprzedazy
            //        r.AddEdge(new Edge(j, sink, 0)); // -sales[pp,j].Value , chyba 0 bo to odprowadza po prostu od sprzedawcy gdzieoklwiek
            //        ++pp;
            //    }
            //    g.AddEdge(new Edge(j, j + 1, storageInfo.Quantity)); // dodawanie magazynu
            //    r.AddEdge(new Edge(j, j + 1, storageInfo.Value));
            //}
            //g.AddEdge(source, n-1, production[n - 1].Quantity); // ostatni tydzien produkcji
            //r.AddEdge(source, n-1, production[n - 1].Value);
            //int p = 0;
            //for (int i = start; i <= end; ++i)
            //{
            //    g.AddEdge(n - 1, i, sales[p, n - 1].Quantity);
            //    r.AddEdge(new Edge(n - 1, i, -sales[p, n - 1].Value));
            //    g.AddEdge(new Edge(n-1, sink, sales[p, n-1].Quantity)); // dodawanie sprzedazy
            //    r.AddEdge(new Edge(n-1, sink, 0)); //-sales[p, n-1].Value
            //    ++p;
            //}
            //(double value, double cost, Graph flow) ret = MinCostFlowGraphExtender.MinCostFlow(g, r, source, sink, false, null, null, false);
            //int produced = 0;
            ////Console.WriteLine("wywala");
            //for (int i = 0; i < n; ++i)
            //{
            //    //Console.WriteLine("tak");
            //    produced += (int)ret.flow.GetEdgeWeight(source, i );

            //    weeklyPlan[i].UnitsProduced = (int)ret.flow.GetEdgeWeight(source, i);

            //    int ppp = 0;
            //    for (int ii = start; ii <= end; ++ii)
            //    {
            //        //Console.WriteLine("{0}, {1},  {2}", ii, ppp, count);
            //        if (!Double.IsNaN(ret.flow.GetEdgeWeight(i, ii)))
            //            weeklyPlan[i].UnitsSold[ppp] = (int)ret.flow.GetEdgeWeight(i, ii);
            //        else
            //            weeklyPlan[i].UnitsSold[ppp] = 0;
            //        //weeklyPlan[i].UnitsSold = (int)ret.flow.GetEdgeWeight(ii + 1, sink);
            //        ++ppp;
            //    }
            //    //Console.WriteLine("tutajtak");
            //    if (i != n - 2)
            //        weeklyPlan[i].UnitsStored = (int)ret.flow.GetEdgeWeight(i , i + 1);
            //    else
            //        weeklyPlan[i].UnitsStored = 0;
            //    //Console.WriteLine("tu");
            //}


            //return new PlanData {Quantity = tmp, Value = -ret.cost};
        }

    }
}